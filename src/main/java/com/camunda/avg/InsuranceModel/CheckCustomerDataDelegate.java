package com.camunda.avg.InsuranceModel;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.connect.Connectors;
import org.camunda.connect.httpclient.HttpConnector;
import org.camunda.connect.httpclient.HttpResponse;

public class CheckCustomerDataDelegate implements JavaDelegate {

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		HttpConnector http = Connectors.getConnector(HttpConnector.ID);
        
		//id des Kunden für URL
		String id = (String) execution.getVariableLocal(execution.getId());
		
        HttpResponse response = http.createRequest()
        		.get()
        		.header("Accept", "application/json")
        		.url("http://localhost:8091/customers/"+ id)
        		.execute();
        
        
        Integer statusCode = response.getStatusCode();
        String contentTypeHeader = response.getHeader("Content-Type");
        String body = response.getResponse();
        
        System.out.println("statusCode: " + statusCode);
        System.out.println("contentTypeHeader: " + contentTypeHeader);
        System.out.println("body: " + body);
        
        //falls Nicht als JSON objekt zurück kommt
        //SpinJsonNode customerData = JSON(body);
        
        //TODO: evtl. Konvertieren zu Kundendaten Klasse
        //			-> body parsen und Werte übernehmen sonst muss an anderer Stelle jedes mal geparst werden
        
        execution.setVariable(execution.getId(), "CustomerData", body);

        //String retrievedOrder = (String) execution.getVariable("CustomerData");
        //
        //Hier wenn zu Klasse konvertiert wurde
        //Object retrievedOrder = execution.getVariableLocal("CustomerData");

        
        response.close();
	}


}


