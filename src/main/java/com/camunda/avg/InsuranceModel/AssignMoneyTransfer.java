package com.camunda.avg.InsuranceModel;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import java.util.logging.Logger;

public class AssignMoneyTransfer implements JavaDelegate {

	private final Logger log = Logger.getLogger( AssignMoneyTransfer.class.getName() );

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		// TODO Auto-generated method stub
		log.info("Die Schadenssumme wurde an den Kunden überwiesen");
	}

}
